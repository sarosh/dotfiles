all:
	rm ~/.bash_aliases
	rm ~/.tmux.conf
	rm ~/.vimrc
	ln -s ~/paperprojects/dotfiles/tmux.conf ~/.tmux.conf
	ln -s ~/paperprojects/dotfiles/bash_aliases ~/.bash_aliases
	ln -s ~/paperprojects/dotfiles/vimrc ~/.vimrc

pdf:
	doconce format pdflatex presentation SLIDE_TYPE="beamer" SLIDE_THEME="re	d_plain" --latex_title_layout=beamer
	doconce ptex2tex presentation envir=minted
	doconce slides_beamer presentation --beamer_slide_theme=red_plain --hand	out  # note --handout!
	pdflatex -shell-escape presentation

html:
	doconce format html presentation --pygments_html_style=perldoc --keep_pygments_html_bg SLIDE_TYPE=reveal SLIDE_THEME=beige
	doconce slides_html presentation reveal --html_slide_theme=beige

