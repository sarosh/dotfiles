**Reduced Order Model of WWW of SAROSH QURAISHI**

-   **Search:**[scirus](http://www.scirus.com/)
    **/**[google](http://www.google.com/) / [arxiv](http://arxiv.org/) /
    [bibliography](http://liinwww.ira.uka.de/bibliography/index.html) /
    [mendeley](http://www.mendeley.com/) /
    [googlecodes](http://code.google.com/) /
    [googletrends](http://www.google.com/trends/)

-   **Libraries/Encyclopedia** [CiteSeer](http://citeseer.ist.psu.edu/)
    / [JSTOR](http://www.jstor.org/) /
    [LIBIS](http://opac.libis.be/F/?func=find-b-0&local_base=OPAC01) /
    [Math websites](http://www.math.psu.edu/MathLists/Contents.html) /
    [MathWorld](http://mathworld.wolfram.com/) / [TW
    Reports](http://www.cs.kuleuven.ac.be/publicaties/rapporten/TW/) /
    [Zentralblatt MATH](http://www.emis.de/ZMATH/) /
    [grabcad](https://grabcad.com/library) /
    [matplotlib](http://matplotlib.org/gallery.html) /
    [shodhganga](http://shodhganga.inflibnet.ac.in/)

-   **News**: [twitter](https://twitter.com/) /

-   **ToDo List:**[wunderlist](https://www.wunderlist.com/#/lists/inbox)
    / doodle /

-   **Email:**[gmail](http://www.gmail.com/)
    **/**[rediffmail](http://www.rediffmail.com/) /
    [TUberlin](https://www3.math.tu-berlin.de/squirrelmail/)

-   **Version control:**
    [bitbucket](https://bitbucket.org/dashboard/overview)

-   **Health:** [webmd](http://www.webmd.com/)

-   **Shopping**: [guenstiger](http://www.guenstiger.de/) /
    [amazon.de](http://amazon.de/) / [kaufda](http://www.kaufda.de/)

-   **Research/Learning:**

o **Methods**: [multigrid](http://www.mgnet.org/) /
[qcmethod](http://www.qcmethod.com/) /
[xfem@aachen](http://www.xfem.rwth-aachen.de/Background/Introduction/XFEM_Introduction.php)
/ [ddm](http://www.ddm.org/) / [compressed
sensing@rice](http://dsp.rice.edu/cs) / [compressed
sensing](http://www.compressedsensing.com/) /
[NuitBlanche](http://nuit-blanche.blogspot.de/)

-    **Programming:**
[learnXinY](https://github.com/adambard/learnxinyminutes-docs)
[matlab2python](http://stackoverflow.com/questions/9845292/a-tool-to-convert-matlab-code-to-python)

-   **Educational materials:**
    [bodysoulmath](http://www.bodysoulmath.org/) /
    [freevideolectures](http://freevideolectures.com/) /
    [nptel](http://nptel.iitm.ac.in/) /
    [](http://ocw.mit.edu/index.html)[MIT
    OCW](http://ocw.mit.edu/index.html) / [MIT
    world](http://mitworld.mit.edu/) /
    [](http://web.sls.csail.mit.edu/lectures/)/
    [](http://www.vega.org.uk/)[Vega Science
    Trust](http://www.vega.org.uk/) /
    [Animations](http://www.maxanim.com/index.htm) /[National Institutes
    of Health](http://videocast.nih.gov/default.asp)/
    [edu@youtube](http://www.youtube.com/edu) /
    [videolectures.net](http://videolectures.net/) /
    [coursera](http://www.coursera.org)/
    [edX](http://www.edx.org) 

-   **Course Slides/Notes:**
    [ECOLE-STRUC_DYN](http://catalogue.polytechnique.fr/site.php?id=371),
    [FARHAT](http://www.stanford.edu/group/frg/course_work/index.html),
    [FARIN](http://www.farinhansford.com/books/pla/downloads.html),
    [BOYD](http://www.stanford.edu/~boyd/teaching.html),
    [Adcock](http://www.math.purdue.edu/~adcock/MA692Fall13/MA692Fall13.html),
    [KUZMIN(CFD)](http://www.mathematik.uni-dortmund.de/~kuzmin/cfdintro/cfd.html),
    [FASSHAUER](http://www.math.iit.edu/~fass/590/index.html),
    [Stevenson](http://staff.science.uva.nl/~rstevens/notes1.pdf),
    [NOWAK](http://nowak.ece.wisc.edu/nteaching.html) (ML),
    [MITCHELL](http://www.cs.cmu.edu/~tom/10701_sp11/lectures.shtml)
    (ML), [CORTES\_LEARNING
    KERNELS](http://www.cs.nyu.edu/~mohri/icml2011-tutorial/),
    [JELANI\_NELSON](http://people.seas.harvard.edu/~minilek/cs229r/lec.html),
    [LANGTANGEN_PYTHON_PRIMER](http://heim.ifi.uio.no/~inf1100/slides/)
    [Scott Rixner_OOProgramming](http://www.clear.rice.edu/comp310/f13/)

-   **TALKS:** [ADCOCK](http://www.math.purdue.edu/~adcock/),
    [HOLLIG](http://www.web-spline.de/publications/index.html),
    [TROPP](http://users.cms.caltech.edu/~jtropp/talks.html),
    [RANDNLA](http://www.cs.rpi.edu//~drinep/RandNLA/),
    [HEGDE](http://people.csail.mit.edu/chinmay/talks.html)
    [WIRTZ-KERNELS FOR MOD](http://www.mathematik.uni-stuttgart.de/fak8/ians/lehrstuhl/agh/orga/people/wirtz/)

-   **Cheetsheets**: [cheatography](http://www.cheatography.com/) /

-   **Forums:**[imechanica](http://www.imechanica.org/) **/**[wavelet
    digest](http://www.wavelet.org/) /

-   **Codes:**[sagenb](http://www.sagenb.org/) /
    [nanohub](http://www.nanohub.org/) /
    [NClab](https://desktop.nclab.com/) /
    [fenics](http://fenicsproject.org/) /
    [MATLABtoPythonConverter](http://ompclib.appspot.com/m2py)

-   **Tools:**[pandoc (document conversion)](http://johnmacfarlane.net/pandoc)

-   **Misc:**[research-tips](http://www.ifs.tuwien.ac.at/~silvia/research-tips/)
    / [nsf](http://www.nsf.gov/index.jsp) /
    [vimbits](http://vimbits.com/) /
    [bestofvim](http://www.bestofvim.com/)

-   **Math Journals :** [APNUM](http://ees.elsevier.com/apnum/) /
    [ETNA](http://etna.mcs.kent.edu/) /
    [JFAA](http://www.springerlink.com/%28awntkb3czbln0fbg50v1kzif%29/app/home/journal.asp?referrer=parent&backto=linkingpublicationresults,1:109375,1)
    /
    [JCAM](http://www.elsevier.com/wps/find/journaldescription.cws_home/505613/description#description)
    / [IJWMIP](http://www.worldscinet.com/journals/ijwmip/ijwmip.shtml)
    / [IMAJNA](http://imanum.oxfordjournals.org/) /
    [SIAM](http://www.siam.org/) /
    [ACM](http://www.springer.com/computer/theoretical+computer+science/journal/10444)
    /
    [JEM](http://www.springer.com/new+&+forthcoming+titles+(default)/journal/10665)
    / IJNME / ONM / IJIAM

-   **Computation Journals :**
    [CMech](http://www.springer.com/engineering/journal/466) / CMAME /
    CCP / [CAGD](http://ees.elsevier.com/cagd/)

-   **Conferences:**[siam
    calendar](http://www.siam.org/meetings/calendar.php)

-   **Randomized NLA:**Avron, Drineas, Mahoney, Halko, Tropp,
    Martinsson, Nelson, Vempula, Rokhlin, Tygert,

-   **CS:** Candes, Gitta,

-   **Wavelet:** [Albert Cohen](http://www.ann.jussieu.fr/%7Ecohen/) /
    [Wolfgang Dahmen](http://www.igpm.rwth-aachen.de/dahmen/) / [Ingrid
    Daubechies](http://fds.duke.edu/db/aas/math/ingrid)/ [Oleg
    Davydov](http://www.maths.strath.ac.uk/%7Eaas04108/) / [Carl De
    Boor](http://www.cs.wisc.edu/%7Edeboor/) / [Ron
    Devore](http://www.math.sc.edu/%7Edevore/) //// [Paul
    Dierckx](http://www.cs.kuleuven.ac.be/%7Epol/) / [David
    Donoho](http://www-stat.stanford.edu/%7Edonoho/) / [Michael
    Floater](http://heim.ifi.uio.no/%7Emichaelf/) / [Tim
    Goodman](http://www.maths.dundee.ac.uk/%7Etgoodman/) / [Bin
    Han](http://www.ualberta.ca/%7Ebhan/) / [Rong-Qing
    Jia](http://www.math.ualberta.ca/%7Ejia/) / [Angela
    Kunoth](http://www.iam.uni-bonn.de/%7Ekunoth/) / [Ming-Jun
    Lai](http://www.math.uga.edu/%7Emjlai/) / [Stephane
    Mallat](http://www.cmap.polytechnique.fr/%7Emallat/) / [Mike
    Neamtu](http://www.math.vanderbilt.edu/%7Eneamtu/) / [Peter
    Oswald](http://www.faculty.iu-bremen.de/poswald/) / [Larry
    Schumaker](http://www.math.vanderbilt.edu/%7Eschumake/) / [Zuowei
    Shen](http://www.math.nus.edu.sg/%7Ematzuows/) / [Rob
    Stevenson](http://www.math.uu.nl/people/stevenso/) / [Gilbert
    Strang](http://www-math.mit.edu/%7Egs/) / [Wim
    Sweldens](http://cm.bell-labs.com/cm/ms/who/wim/) / [Evelyne
    Vanraes](http://www.cs.kuleuven.ac.be/%7Eevelyne) / T. Dijkema /
    [Jan Maes](http://users.fulladsl.be/spb24127/) / A Schneider /
    [Miriam
    Primbs](http://www.uni-due.de/~hm0029/primbs/Miriam_Primbs.html) /
    [More…](http://faculty.cs.tamu.edu/klappi/people.html)

-   Conference funding: NBHM (for IMU/ICIAM) / DST / INSA for IMU /
    [CCSTDS](http://www.ccstds.tn.nic.in/html/fellowship_for_indian_national.html)
    /CSIR

-   **Jobs/Awards:
    [EURAXESS](http://ec.europa.eu/euraxess/index.cfm/jobs/),**[usacm](http://www.usacm.org/career.htm)
    / [usief](http://www.usief.org.in/) /
    [ukjobs@timeshighereducation](http://www.timeshighereducation.co.uk/jobs_seo_role.asp?navcode=2)
    /
    [scholarship-positions](http://scholarship-positions.com/?gclid=CKHrkbGDupsCFdgtpAod2jZ52w)
    / [phds.org](http://www.phds.org/) /
    [fellowship@indousstf](http://www.indousstf.org/fellowship.htm) /
    [NAS@India](http://nasi.nic.in/Granting%20Agencies%20Awards.htm) /
    [academicjobs](http://engineering.academickeys.com/seeker_job.php) /
    [postdoc@epsrc](http://www.epsrc.ac.uk/funding/fellows/Pages/postdoctoral.aspx)
    / [sarkari-naukri](http://www.sarkari-naukri.blogspot.com/) /
    [scholarship.canadianstudies](http://www.jmi.ac.in/oir/oir.scholarship.canadianstudies.htm)
    / [scholarshipnet](http://www.scholarshipnet.info/category/postdoc/)
    /
    [MarieCurie\_sweden](http://nextscholarships.com/degree/doctoral/the-fas-marie-curie-international-postdoc-programme-for-internationalisation.html)/
    [euraxess](http://ec.europa.eu/euraxess/index.cfm/jobs/index) /
    [Postdocjobs](http://www.postdocjobs.com/) /
    [findapostdoc](http://www.findapostdoc.com/) /
    [royalsociety](http://royalsociety.org/funding-scientists/?from=footer)
    /
    [post-docs.com](http://www.post-docs.com/jobs/jobs2.php?subcatid=38&catid=2)
    / unjobs.org

-   **Tennis:**[fuzzyyellowballs](http://www.fuzzyyellowballs.com/) /
    [usta](http://www.usta.com/ImproveYourGame/Default.aspx) /
    [tennis-warehouse](http://twu.tennis-warehouse.com/learning_center/index.php)

-   **About Me:**
    [Papers](http://www.mendeley.com/profiles/sarosh-quraishi/) /
    Personal / Family…

**Updated: November 22, 2013**


