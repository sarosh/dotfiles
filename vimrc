set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.

" Basic Plugins
Plugin 'tpope/vim-sensible'
"color scheme
Plugin 'nanotech/jellybeans.vim'
"status line
Plugin 'bling/vim-airline'
"syntax checker
Plugin 'scrooloose/syntastic'
" Yank history
Plugin 'YankRing.vim'
" navigation
Plugin 'majutsushi/tagbar'
Plugin 'scrooloose/nerdtree'

"Plugin related to snippets and word completion 
Plugin 'honza/vim-snippets'
Plugin 'SirVer/ultisnips' "depends on existing snippets
" writing related general purpose coding
Plugin 'tpope/vim-surround'
Plugin 'tomtom/tcomment_vim'
" pandoc related:
Plugin 'vim-pandoc/vim-pandoc'
Plugin 'vim-pandoc/vim-pandoc-syntax'
"Latex related
Plugin 'lervag/vim-latex' 

"python related:
Plugin 'klen/python-mode'
Plugin 'jmcantrell/vim-virtualenv'
Plugin 'jaredly/vim-debug'
Plugin 'tell-k/vim-autopep8'


" All of your Plugins must be added before the following line
call vundle#end()            " required
"""------------------------------------------------------------------------
filetype plugin indent on    " required


let mapleader=","
nmap <leader>e :!python %<CR>
nmap <leader>E :!/usr/bin/python %<CR>
" Automatic reloading of .vimrc
augroup reload_vimrc " {
    autocmd!
    autocmd BufWritePost $MYVIMRC source $MYVIMRC
augroup END " }
"autocmd BufWritePost .vimrc source %
color jellybeans

set path+=$PWD/**      
set cursorline
set expandtab
set modelines=0
set shiftwidth=2
set pastetoggle=<F2>
set clipboard=unnamed
set synmaxcol=128
set ttyscroll=10
set encoding=utf-8
set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround
set wrap
set number
set expandtab
set nowritebackup
set noswapfile
set nobackup
set hlsearch
set ignorecase
set smartcase
set foldmethod=indent
set ruler
set autochdir
set spell spelllang=en_gb
set nospell
set shell=/bin/bash\ -i
set tags=./tags,tags;$HOME
"todo: explain what the following line does?
nnoremap <space> za
vnoremap <space> zf

" Key remaps
nmap <F2> :NERDTreeToggle<CR>
nmap <F3> :TagbarToggle<CR>

nmap <F8> :PymodeLintAuto<CR>
nmap <F9> :TagbarToggle<CR>


" Automatic formatting
autocmd BufWritePre *.rb :%s/\s\+$//e
autocmd BufWritePre *.go :%s/\s\+$//e
autocmd BufWritePre *.haml :%s/\s\+$//e
autocmd BufWritePre *.html :%s/\s\+$//e
autocmd BufWritePre *.scss :%s/\s\+$//e
autocmd BufWritePre *.slim :%s/\s\+$//e

au BufNewFile * set noeol
au BufRead,BufNewFile *.go set filetype=go
"set spell checker for tex files
autocmd BufRead,BufNewFile *.tex setlocal spell
" No show command
autocmd VimEnter * set nosc

" Quick ESC
imap jj <ESC>

" Quicksave command
"noremap <C-Z> :update<CR>
"vnoremap <C-Z> <C-C> :update<CR>
"inoremap <C-Z> <C-O> :update<CR>
"Quickly change the pwd to current files paths
nnoremap ,cd :cd %:p:h<CR>:pwd<CR>

"Quickly open vim (edit vim) in vertical split
nnoremap <leader>ev :vsplit $MYVIMRC<cr>

" easier moving of code blocks
vnoremap < <gv " better indentation
vnoremap > >gv " better indentation


" Jump to the next row on long lines
map <Down> gj
map <Up>   gk
nnoremap j gj
nnoremap k gk

" format the entire file
nmap <leader>fef ggVG=

" Open new buffers
nmap <leader>s<left>   :leftabove  vnew<cr>
nmap <leader>s<right>  :rightbelow vnew<cr>
nmap <leader>s<up>     :leftabove  new<cr>
nmap <leader>s<down>   :rightbelow new<cr>

" Tab between buffers
noremap <tab> <c-w><c-w>

" Switch between last two buffers
nnoremap <leader><leader> <C-^>

" Resize buffers
if bufwinnr(1)
  nmap Ä <C-W><<C-W><
  nmap Ö <C-W>><C-W>>
  nmap ö <C-W>-<C-W>-
  nmap ä <C-W>+<C-W>+
endif

"--------------------------------------------------
" Ultisnips
" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

"================================
"python
"python-mode (https://github.com/vim-scripts/Python-mode-klen#settings)
" Load the whole plugin
let g:pymode = 1

" Load and show documentation plugin
let g:pymode_doc = 1
let g:pymode_doc_key = 'K'

" Load and run code plugin
let g:pymode_run = 1
let g:pymode_run_key = '<leader>r'

" Load and set breakpoints plugin
let g:pymode_breakpoint = 1
let g:pymode_breakpoint_key = '<leader>b'

" Enable python code folding
let g:pymode_folding = 1
" Enable python objects and motion
let g:pymode_motion = 1
" Auto fix vim python paths if virtualenv enabled
let g:pymode_virtualenv = 1
" Additional python paths
let g:pymode_paths = []

" Autoremove unused whitespaces
let g:pymode_utils_whitespaces = 1

" Enable pymode indentation
let g:pymode_indent = 1

" Set default pymode python options
let g:pymode_options = 1

" Load pylint code plugin
let g:pymode_lint = 1
" Switch pylint, pyflakes, pep8, mccabe code-checkers
" Can have multiple values "pep8,pyflakes,mcccabe"
" Choices are: pyflakes, pep8, mccabe, pylint, pep257
let g:pymode_lint_checker = "pylint,pyflakes,pep8,mccabe"

" Skip errors and warnings
" E.g. "E501,W002", "E2,W" (Skip all Warnings and Errors startswith E2) and etc
let g:pymode_lint_ignore = "E501"

" Select errors and warnings
" E.g. "E4,W"
let g:pymode_lint_select = ""

" Run linter on the fly
let g:pymode_lint_onfly = 0

" Pylint configuration file
" If file not found use 'pylintrc' from python-mode plugin directory
let g:pymode_lint_config = "$HOME/.pylintrc"

" Check code every save
let g:pymode_lint_write = 1

" Auto open cwindow if errors were found
let g:pymode_lint_cwindow = 1

" Show error message if cursor placed at the error line
let g:pymode_lint_message = 1

" Auto jump on first error
let g:pymode_lint_jump = 0

" Hold cursor in current window
" when quickfix is open
let g:pymode_lint_hold = 0

" Place error signs
let g:pymode_lint_signs = 1

" Maximum allowed mccabe complexity
let g:pymode_lint_mccabe_complexity = 8

" Minimal height of pylint error window
let g:pymode_lint_minheight = 3

" Maximal height of pylint error window
let g:pymode_lint_maxheight = 6

" Symbol definition
let g:pymode_lint_todo_symbol = 'WW'
let g:pymode_lint_comment_symbol = 'CC'
let g:pymode_lint_visual_symbol = 'RR'
let g:pymode_lint_error_symbol = 'EE'
let g:pymode_lint_info_symbol = 'II'
let g:pymode_lint_pyflakes_symbol = 'FF'
" Load rope plugin
let g:pymode_rope = 1

" Map keys for autocompletion
let g:pymode_rope_autocomplete_map = '<C-Space>'

" Auto create and open ropeproject
let g:pymode_rope_auto_project = 1

" Enable autoimport
let g:pymode_rope_enable_autoimport = 1

" Auto generate global cache
let g:pymode_rope_autoimport_generate = 1

let g:pymode_rope_autoimport_underlineds = 0

let g:pymode_rope_codeassist_maxfixes = 10

let g:pymode_rope_sorted_completions = 1

let g:pymode_rope_extended_complete = 1

let g:pymode_rope_autoimport_modules = ["os","shutil","datetime"]

let g:pymode_rope_confirm_saving = 1

let g:pymode_rope_global_prefix = "<C-x>p"

let g:pymode_rope_local_prefix = "<C-c>r"

let g:pymode_rope_vim_completion = 1

let g:pymode_rope_guess_project = 1

let g:pymode_rope_goto_def_newwin = ""

let g:pymode_rope_always_show_complete_menu = 0
" Enable pymode's custom syntax highlighting
let g:pymode_syntax = 1

" Enable all python highlightings
let g:pymode_syntax_all = 1

" Highlight "print" as a function
let g:pymode_syntax_print_as_function = 0

" Highlight indentation errors
let g:pymode_syntax_indent_errors = g:pymode_syntax_all

" Highlight trailing spaces
let g:pymode_syntax_space_errors = g:pymode_syntax_all

" Highlight string formatting
let g:pymode_syntax_string_formatting = g:pymode_syntax_all

" Highlight str.format syntax
let g:pymode_syntax_string_format = g:pymode_syntax_all

" Highlight string.Template syntax
let g:pymode_syntax_string_templates = g:pymode_syntax_all

" Highlight doc-tests
let g:pymode_syntax_doctests = g:pymode_syntax_all

" Highlight builtin objects (__doc__, self, etc)
let g:pymode_syntax_builtin_objs = g:pymode_syntax_all

" Highlight builtin functions
let g:pymode_syntax_builtin_funcs = g:pymode_syntax_all

" Highlight exceptions
let g:pymode_syntax_highlight_exceptions = g:pymode_syntax_all

" Highlight equal operator
let g:pymode_syntax_highlight_equal_operator = g:pymode_syntax_all

" Highlight stars operator
let g:pymode_syntax_highlight_stars_operator = g:pymode_syntax_all

" Highlight `self`
let g:pymode_syntax_highlight_self = g:pymode_syntax_all


"vim-latex
let g:latex_mappings_enabled=1
let g:latex_enabled=1
let g:latex_build_dir = '.'
let g:latex_quickfix_ignored_warnings = [
        \ 'Underfull',
        \ 'Overfull',
        \ 'specifier changed to',
      \ ]
let g:latex_toc_enabled = 1
let g:latex_viewer = 'okular'

