TITLE: My personal notebook: (I) ideas, (TD) things to do, (HT) how to do
AUTHOR: Sarosh.Quraishi@gmail.com at Technical University-Berlin
DATE: Today




!split
===== (HT) How to increase speed of python code ===== 
!bblock
    * python for loop is slow 
    * Choice1: vectorize: replace it by 'numpy' array operations eg: 'np.sum' etc 
        * Caveat: dont vectorize partially
    * Choice2: cy1: migrate scalar python to cython
        * all variables are type declared e.g. cdef int $M=0$
        * code put in seperate .pyx file
        * running cython on .pyx turns it into C
        * compile and link C to form a shared library
        * import it in python
        * all of this can be done in setup.py script
        * Caveat: profiling is needed as resulting cython code is not always more efficient
    * Choice3: cy2:  
!eblock
!eslidecell

!split
===== (HT) Get Pmat and Qmat for hierarchical basis =====
!bblock
* We have coarse scale nodal $\Phi^0$, fine scale nodal $\Phi^1$ and fine scale bubble (hierarchical) $\Psi^1$, which is subsampled $\Phi^1$
* We also have coarse scale mesh points $T^0$ and fine scale mesh points $T^1$
* (I) Evaluate these basis functions at nodes
* For Pmat
    * evaluate array of $\Phi^0$ at $T^1$ and put in Pmat
* For Qmat
    * evaluate $\Phi^1$ at bubble points $T^2$ $\$ $T^1$ and put in Qmat
!eblock

!split
===== References ===== 
* Nils Gr\"{a}bner, SQ, Christian Schr\"{o}der, Volker Mehrmann and Utz von Wagner.  New numerical methods for the complex eigenvalue analysis of disk brake squeal. In Proceedings from EuroBrake 2014.
* SQ, Christian Schr\"{o}der, and Volker Mehrmann. Model reduction for parameter dependent mechanical systems. In GAMM conference 2014, Erlangen, Germany (to appear).
* Nils Gr\"{a}bner, Volker Mehrmann, SQ, Christian Schr\"{o}der and Utz von Wagner. Numerical methods for parametric model reduction in the simulation of disc brake squeal (Technical report, under preparation)

